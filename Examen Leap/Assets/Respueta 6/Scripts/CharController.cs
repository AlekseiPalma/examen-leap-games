﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharController : MonoBehaviour
{
    public float Speed;
    public float rollSpeed;
    public float jumpForce;

    public int atkCounter = 1;

    public bool Jumping, Rolling, Attacking;

    float distanceGround;

    Rigidbody rb;
    Animator anim;

    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody>();
        anim = GetComponent<Animator>();
        distanceGround = GetComponent<Collider>().bounds.extents.y;
    }

    // Update is called once per frame
    void Update()
    {
        GroundCheck();

        if (!Rolling && !Attacking)
        {
            Attack();
        }

        if(Attacking)
        {
            rb.velocity = Vector3.zero;
        }

        if (Input.GetButtonDown("Fire3") && Rolling == false && Jumping == false)
        {
            StopCoroutine("Roll");
            StartCoroutine("Roll", 0.5f);
        }

        if (Input.GetButtonDown("Jump") && Jumping == false)
        {
            Attacking = false;
            rb.velocity = new Vector3(rb.velocity.x, jumpForce, rb.velocity.z);
            Jumping = true;
            anim.Play("Jump", 0);
        }
    }

    private void FixedUpdate()
    {
        if (!Attacking && !Rolling)
        {
            Movement();
        }

        if (Rolling == true)
        {
            rb.velocity = transform.TransformDirection(Vector3.right * rollSpeed);
        }
    }

    void Attack()
    {
        switch (atkCounter)
        {
            case 1:
                if (Input.GetButtonDown("Fire1"))
                {
                    StopCoroutine("ResetTimer");

                    Attacking = true;

                    if (!Jumping)
                    {
                        anim.Play("G-Atk1", 0);
                    }
                    else
                    {
                        anim.Play("A-Atk1", 0);
                    }

                    atkCounter++;
                }
                else if (Input.GetButtonDown("Fire2") && Jumping == false)
                {
                    StopCoroutine("ResetTimer");

                    Attacking = true;

                    anim.Play("R-Atk1", 0);
                    atkCounter = 2;
                }
                break;

            case 2:
                if (Input.GetButtonDown("Fire1"))
                {
                    StopCoroutine("ResetTimer");

                    Attacking = true;

                    if (!Jumping)
                    {
                        anim.Play("G-Atk2", 0);
                    }
                    else
                    {
                        anim.Play("A-Atk2", 0);
                    }

                    atkCounter++;
                }
                else if (Input.GetButtonDown("Fire2") && Jumping == false)
                {
                    StopCoroutine("ResetTimer");

                    Attacking = true;

                    anim.Play("R-Atk2", 0);
                    atkCounter = 1;
                }
                break;

            case 3:
                if (Input.GetButtonDown("Fire1"))
                {
                    StopCoroutine("ResetTimer");

                    Attacking = true;

                    if (!Jumping)
                    {
                        anim.Play("G-Atk3", 0);
                    }
                    else
                    {
                        anim.Play("A-Atk3", 0);
                    }

                    atkCounter = 1;
                }
                else if (Input.GetButtonDown("Fire2") && Jumping == false)
                {
                    StopCoroutine("ResetTimer");

                    Attacking = true;

                    anim.Play("R-Atk1", 0);
                    atkCounter = 2;
                }
                break;
        }
    }

    void ResumeAtk()
    {
        Attacking = false;
        StartCoroutine("ResetTimer", 0.5f);
    }

    private IEnumerator ResetTimer(float timer)
    {
        yield return new WaitForSeconds(timer);

        atkCounter = 1;
    }
    
    void Movement()
    {
        float h = Input.GetAxis("Horizontal");
        float v = Input.GetAxis("Vertical");

        if (h > 0)
        {
            transform.rotation = Quaternion.Euler(0, 0, 0);
        }
        else if (h < 0)
        {
            transform.rotation = Quaternion.Euler(0, 180, 0);
        }

        if(h != 0 || v != 0)
        {
            anim.SetBool("Running", true);
        }
        else if(h == 0 && v == 0)
        {
            anim.SetBool("Running", false);
        }

        rb.velocity = new Vector3(h * Speed, rb.velocity.y, v * Speed);
    }

    private IEnumerator Roll(float rollTime)
    {
        Attacking = false;
        anim.Play("Roll", 0);
        Rolling = true;

        yield return new WaitForSeconds(rollTime);

        Rolling = false;
        rb.velocity = Vector3.zero;
    }

    private void GroundCheck()
    {
        RaycastHit hit;

        if (!Physics.Raycast(transform.position, -Vector3.up, out hit, distanceGround + 0.1f))
        {
            anim.SetBool("OnAir", true);
            Jumping = true;

        }
        else
        {
            anim.SetBool("OnAir", false);
            Jumping = false;
        }
    } 

}
