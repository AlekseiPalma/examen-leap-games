﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class LockOnSytem : MonoBehaviour
{
    List<GameObject> Enemies = new List<GameObject>();
    public List<Enemy> activeEnemies = new List<Enemy>();

    Vector3 pos;
    Vector3 forward;

    // Start is called before the first frame update
    void Start()
    {
        ReCheckEnemies();
    }

    // Update is called once per frame
    void Update()
    {
        pos = transform.position;
        forward = transform.TransformDirection(Vector3.right);


        if (Input.GetKeyDown(KeyCode.K))
        {
            ReCheckEnemies();
        }

        for(int i = Enemies.Count - 1; i >=0; i--)
        {
            if (Enemies[i].GetComponent<Enemy>().Visible == true)
            {
                if(!activeEnemies.Contains(Enemies[i].GetComponent<Enemy>()))
                {
                    activeEnemies.Add(Enemies[i].GetComponent<Enemy>());
                }
            }
            else
            {
                activeEnemies.Remove(Enemies[i].GetComponent<Enemy>());
            }
        }
    }

    public void ReCheckEnemies()
    {
        Enemies.AddRange(GameObject.FindGameObjectsWithTag("Enemy"));
        Enemies = Enemies.Distinct().ToList();
    }
}
