﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : MonoBehaviour
{
    Renderer m_Renderer;
    public bool Visible;
    Vector3 pos;    

    // Start is called before the first frame update
    void Start()
    {
        m_Renderer = GetComponent<Renderer>();
    }

    // Update is called once per frame
    void Update()
    {
        pos = transform.position;

        if(m_Renderer.isVisible)
        {
            Visible = true;
        }
        else
        {
            Visible = false;
        }
    }
}
