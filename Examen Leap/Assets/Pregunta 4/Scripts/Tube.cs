﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Tube : MonoBehaviour
{
    public bool Connected;

    public Connection[] connections;

    public int number;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        switch(connections.Length)
        {
            case 1:
                if(connections[0].Connected == true)
                {
                    Connected = true;
                }
                else
                {
                    Connected = false;
                }
                break;

            case 2:
                if (connections[0].Connected == true && connections[1].Connected == true)
                {
                    Connected = true;
                }
                else
                {
                    Connected = false;
                }
                break;
        }
    }

    public void Rotate()
    {
        transform.Rotate(0, 0, -90);

        /*switch(transform.rotation.z)
        {
            case 0:
                transform.rotation = Quaternion.Euler(0,0,-90);
                break;

            case -90:
                transform.rotation = Quaternion.Euler(0, 0, -180);
                break;

            case -180:
                transform.rotation = Quaternion.Euler(0, 0, -270);
                break;

            case -270:
                transform.rotation = Quaternion.Euler(0, 0, 0);
                break;
        }*/
    }
}
