﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Manager : MonoBehaviour
{
    public List<GameObject> Tubes = new List<GameObject>();

    public int[] correctRot;

    private bool Victory()
    {
        for (int i = Tubes.Count - 1; i >= 0; i--)
        {
            if (Tubes[i].transform.rotation.z != correctRot[i] && Tubes[i].GetComponent<Tube>().Connected == false)
            {
                return false;
            }
        }

        return true;
    }

    // Start is called before the first frame update
    void Start()
    {
        Tubes.AddRange(GameObject.FindGameObjectsWithTag("Tube"));
        Tubes.Sort(
            delegate(GameObject t1, GameObject t2)
            {
                return t1.name.CompareTo(t2.name);
            }
            
            );
    }

    // Update is called once per frame
    void Update()
    {
        if(Victory())
        {
            Debug.Log("Victoria");
        }
    }
}
